#!/bin/bash


EXCHANGE_METHOD="u64 per_page"

# CPU_FREQ_MHZ=2600 # if the cpu is 2600MHz
# The following might only work on Intel CPUs.
CPU_FREQ_MHZ=$(lscpu | grep GHz | awk '{print $NF}' | cut -d"G" -f 1)
CPU_FREQ_MHZ=$(echo ${CPU_FREQ_MHZ}*1000|bc)

echo "Throughput (GB/s)"
echo "|2mb_page_order|0    |1    |2    |3    |4    |5    |6    |7    |8    |9"

for METHOD in ${EXCHANGE_METHOD}; do
    printf "|%14s|" "${METHOD}"
    for i in $(seq 0 9); do
        T=$(grep -A 1 "Total_cycles" stats_2mb_${METHOD}_exchange/seq_1_page_order_${i}_exchange_no_batch | grep "[0-9]\+" | awk -v order="$i" -v freq="${CPU_FREQ_MHZ}" 'BEGIN{SUM=0;N=0;}{SUM+=($1+0)/freq;N+=1;}END{printf("%.2f",4*2^(order+0)*1000000/1024/(SUM/N))}')
        echo -n "${T} |"
    done
    echo
done
